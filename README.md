# Beispiele für erweitertes Markdown

### Referenzen
* [Docs](https://shd101wyy.github.io/markdown-preview-enhanced/#/)
* [GitHub](https://github.com/shd101wyy/markdown-preview-enhanced)
* [Presentation - Slides with reveal.js](https://rawgit.com/shd101wyy/markdown-preview-enhanced/master/docs/presentation-intro.html)
* [Diagramme - Graphviz, Balken, Skizzen, …](https://shd101wyy.github.io/markdown-preview-enhanced/#/diagrams)
* [File imports](https://shd101wyy.github.io/markdown-preview-enhanced/#/file-imports)

### Generierte Inhalte
* [Kurs als Ebook](https://axel-klinger.gitlab.io/enhanced-markdown-test/course.epub) // geht noch nicht!
* [Kurs als PDF](https://axel-klinger.gitlab.io/enhanced-markdown-test/course.pdf)
* [Kurs als HTML](https://axel-klinger.gitlab.io/enhanced-markdown-test/course.html)


### Lizenzhinweis

Diese Vorlage für OER Kurse ist freigegeben unter CC-0 / Public domain. Die Inhalte des Kurses unterliegen der jeweiligen Lizenz, wie sie am Ende der generierten Dateien bzw. in der metadata.yml angegeben sind.
